﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveUp : MonoBehaviour
{
    public float changeAmount = 1;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void moveUp()
    {
        transform.Translate (0, changeAmount, 0);
        changeAmount = -changeAmount;
    }
}
